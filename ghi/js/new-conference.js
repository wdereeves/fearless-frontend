window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = 'http://localhost:8000/api/locations/';

    // Fetch available locations to populate the dropdown
    const locationResponse = await fetch(locationUrl);

    if (locationResponse.ok) {
        const locationData = await locationResponse.json();

        // Get the select element within the form
        const locationSelectTag = document.getElementById('location');
        for (let location of locationData.locations) {
            const optionElement = document.createElement('option');
            optionElement.value = location.id;
            optionElement.innerHTML = location.id;
            locationSelectTag.appendChild(optionElement);
        }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log('Conference created successfully:', newConference);
        } else {
            console.error('Error creating conference:', response.status);
        }
    });
});
